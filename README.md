# README 

## What is this repository for? 

- Source code on Node API Schema Validation with Joi.
- Joi is the most powerful schema description language and data validator for JavaScript.
- Installing Joi is quite easy. 
- Run `npm install joi`  
- Reading - https://joi.dev/ , https://www.npmjs.com/package/joi
- Version - 1.0.0

## Summary of set up 
  - Clone the repository into a new directory on your machine
  - Run `git clone https://purusewda@bitbucket.org/purusewda/joi-node-app.git`  
  - Run the following command from the new directory to install the dependencies: `npm install`
  - The app will run on port 3000 by default. If you would prefer another port, you can specify it in the app.js file. 
  - Look for the given line in the file and replace 3000 with your desired port.
   ` const port = process.env.NODE_ENV || 3000;`
  
  - Finally, start the demo app by running the following command: ` npm start`

## Who do I talk to? 

* Repo owner or admin - Puru Sewda