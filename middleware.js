const Joi = require('joi');
const _ = require('lodash');

const middleware = (schema, property) => {
    return (req, res, next) => {

        const _validationOptions = {
            abortEarly: false, // abort after the last validation error
            allowUnknown: true, // allow unknown keys that will be ignored
            stripUnknown: true // remove unknown keys from the validated data
        };

        const { error } = Joi.validate(req[property], schema, _validationOptions);

        const valid = error == null;
        if (valid) {
            next();
        } else {
    
            const { details } = error;
            const message = details.map(i => i.message).join(',');
            // console.log("error", message);

            res.status(422).json({
                status: 'failed',
                err: {
                    original: error._object,
                    
                    // fetch only message and type from each error
                    details: _.map(error.details, ({message, type}) => ({
                        message: message.replace(/['"]/g, ''),
                        type
                    }))
                }
            })
        }
  }
}

module.exports = middleware;